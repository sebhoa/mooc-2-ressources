**Thématique :** Algorithmique

**Notions liées :** Structures de données, Réseaux

**Résumé de l’activité :** Mise en oeuvre commentée de l'algorithme de Dijkstra dans une courte vidéo introductive. Suite d'exercices sur la recherche de chemin sur d'autres graphes.

**Objectifs :** Comprendre l'algorithme de Dijkstra et la recherche de chemins en général, application à diverses situations (recherche d'un intinéraire sur une carte routière, recherche de chemin dans un routage internet)

**Auteur :** Maxime Fourny

**Durée de l’activité :** 2h 

**Forme de participation :** individuelle, en autonomie, collective 

**Matériel nécessaire :** Papier, stylo !

**Préparation :** Aucune

**Autres références :** 

**Fiche élève cours :** 

**Fiche élève activité :** 
    
    - la vidéo [introduction à Dijkstra](https://www.youtube.com/watch?v=9YBq5bcw1pU)
    - la suite de l'activité est ici : 